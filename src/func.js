const getSum = (str1, str2) => {
  const onlyNumbers = (string) => {
    if (string.length == 0) {
      return true;
    }
    return parseInt(string) == string
  }
  const setZero = (string) => {
    return string.length == 0 ? 0 : string;
  }
  if (typeof (str1) != 'string' || typeof (str2) != 'string') {
    return false;
  }
  if (!onlyNumbers(str1) || !onlyNumbers(str2)) {
    return false;
  }
  str1 = setZero(str1);
  str2 = setZero(str2);

  return (parseInt(str1) + parseInt(str2)).toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let result = { Post: 0, comments: 0 };
  result.Post = listOfPosts.filter(x => x.author == authorName).length;
  result.comments = listOfPosts
    .flatMap(x => x.comments)
    .filter(x => x != undefined && x.author == authorName).length;
  
  return `Post:${result.Post},comments:${result.comments}`;
}

const tickets = (people) => {
  let money = 0;
  for (const man of people) {
    if (man - 25 > money) {
      return 'NO';
    }
    money += 25;
    money -= man - 25;
  }
  return 'YES';
}


module.exports = { getSum, getQuantityPostsByAuthor, tickets };


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
